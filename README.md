Steps to build and execute the application:

Make sure you are in the root directory of the app and run the following commands:

* Build the app

      mvn clean install

* Run the app

      java -jar target/ com.williams.sonoma-1.0-SNAPSHOT.jar
