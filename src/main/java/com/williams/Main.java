package com.williams;

import java.util.Arrays;

/**
 * Author: Anil
 *
 */
public class Main
{
    public static int[][] mergeZipCodes(int[][] zipCodes) {
        //If only one range is given, return that itself
        if(zipCodes.length <= 1) {
            return zipCodes;
        }

        int[][] result = new int[zipCodes.length][2];
        int count = 0,i = 1;
        int[] prev = new int[2];
        int[] curr = new int[2];

        if(zipCodes.length <=0) {
            return new int[][]{};
        }
        // Sort the ZipCode ranges such that they are in ascending order based on first element in range
        // Leveraging lambda functions from Java8
        Arrays.sort(zipCodes, (a, b) -> Integer.compare(a[0],b[0]));
        //Initially store the first range item
        prev = zipCodes[0];

        while(i < zipCodes.length) {
            curr = zipCodes[i];
            //For each range check whether the max element in range is part of the next range
            //If the condition satisfies then merge the range with minimum of first element and maximum of last element
            if(curr[0] <= prev[1]) {
                prev = new int[]{Math.min(prev[0], curr[0]),Math.max(curr[1], prev[1])};
                i++;
            } //Store the range as it is if the above condition is not satisfied
            else {
                result[count] = prev;
                count++;
                prev = curr;
                i++;
            }
        }
        result[count] = prev;
        count++;

        int[][] arr = new int[count][2];
        for(int j = 0;j < count;j++) {
            arr[j] = result[j];
        }
        return arr;
    }

    public static void main( String[] args )
    {
        //Sample Range to make sure the logic is working
        int[][] listOfRanges = new int[][]{{94133,94133}, {94200,94299},{94226,94399}};
        int[][] result = mergeZipCodes(listOfRanges);

        //Printing out the results in user friendly manner
        for(int i = 0; i < result.length;i++) {
            System.out.println("[" + result[i][0] + "," + result[i][1] + "]");
        }
    }
}
