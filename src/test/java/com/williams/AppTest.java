package com.williams;

import static com.williams.Main.mergeZipCodes;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for the MergeZipCode Range App
 */
public class AppTest 
{
    /**
     * Test cases the various ZipCode ranges
     */
    @Test
    public void simpleCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{94133,94133}, {94200,94299},{94499,94699}});

        Assert.assertEquals(new int[][]{{94133,94133}, {94200,94299},{94499,94699} }, listOfRanges);
    }

    @Test
    public void mergedCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{94133,94133}, {94200,94299},{94226,94399}});

        Assert.assertEquals(new int[][]{{94133,94133}, {94200,94399}}, listOfRanges);
    }
    @Test
    public void sameRangeCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{11111,22222}, {11111,22222},{11111,22222}});

        Assert.assertEquals(new int[][]{{11111,22222}}, listOfRanges);
    }
    @Test
    public void identityCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{99999,99999}, {99999,99999},{99999,99999}});

        Assert.assertEquals(new int[][]{{99999,99999}}, listOfRanges);
    }

    @Test
    public void jumbledCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{11115,11117},{11112,11114},{11119,11122},{10000,20000}});

        Assert.assertEquals(new int[][]{{10000,20000}}, listOfRanges);
    }

    @Test
    public void emptyCase() {
        int[][] listOfRanges = mergeZipCodes(new int[][]{{}});

        Assert.assertEquals(new int[][]{{}}, listOfRanges);
    }
}
